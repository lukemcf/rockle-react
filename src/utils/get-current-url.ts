const getCurrentUrl = (): string => document.location.href

export default getCurrentUrl
