type QueryParams = { [key: string]: string }

const getQueryParams = (): QueryParams => {
  const url = new URL(document.location.href)
  return Object.fromEntries(url.searchParams)
}

const mergeQueryParams = (params: QueryParams): string => {
  const existingParams = getQueryParams()
  const newParams = Object.assign(existingParams, params)
  const urlSearchParams = new URLSearchParams(newParams)

  return `${document.location.origin}/?${urlSearchParams.toString()}`
}

export { getQueryParams, mergeQueryParams }
