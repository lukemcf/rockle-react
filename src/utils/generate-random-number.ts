function generateRandomNumber(max: number): number {
  return Math.floor(Math.random() * max);
}

export default generateRandomNumber
