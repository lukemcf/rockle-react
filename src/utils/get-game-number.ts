import { DateTime } from 'luxon'
import { getStartDate } from '../constants'

const getGameNumber = (): number => {
  const startDate = getStartDate()
  const startOfToday = DateTime.local().startOf('day')

  return startOfToday.diff(startDate, 'days').as('days') + 1
}

export default getGameNumber
