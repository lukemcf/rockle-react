import songs, { Song } from '../data/songs'

const getSong = (gameNumber: number): Song => {
  const song = songs[gameNumber - 1]
  if (!song?.videoId) {
    throw new Error('no song')
  }

  return song
}

export default getSong



