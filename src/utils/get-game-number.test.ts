import getGameNumber from './get-game-number'
import { DateTime, Settings } from 'luxon'
import * as constants from '../constants'

describe('getGameNumber', () => {
  it('returns number of days since start date', () => {
    Settings.now = () => new Date(2022, 2, 15, 23).getTime()
    jest.spyOn(constants, 'getStartDate').mockReturnValue(
      DateTime.local(2022, 3, 1)
    )
    expect(getGameNumber()).toEqual(15)

    Settings.now = () => new Date(2023, 2, 1, 12).getTime()
    jest.spyOn(constants, 'getStartDate').mockReturnValue(
      DateTime.local(2022, 3, 1)
    )
    expect(getGameNumber()).toEqual(366)
  })
})
