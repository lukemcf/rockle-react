// pass through function so that we can mock navigator.share
const share = (data?: ShareData): Promise<void> => navigator.share(data)

export default share
