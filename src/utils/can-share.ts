// pass through function so that we can mock navigator.canShare
const canShare = (data?: ShareData): boolean => !!navigator.canShare && navigator.canShare(data)

export default canShare
