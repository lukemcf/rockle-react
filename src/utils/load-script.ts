const loadScript = (src: string): void => {
  const existingScript = document.querySelector(`[src="${src}"]`)

  if (!existingScript) {
    const script = document.createElement('script');
    script.src = src
    document.body.appendChild(script);
  }
}

export default loadScript
