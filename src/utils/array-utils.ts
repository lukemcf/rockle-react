const uniq: <T>(arr: T[]) => T[] = arr => Array.from(new Set(arr))

export { uniq }
