import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import { getQueryParams } from './utils/query-params'

import './index.css'

ReactDOM.render(
  <React.StrictMode>
    <App queryParams={getQueryParams()} />
  </React.StrictMode>,
  document.getElementById('root'),
)
