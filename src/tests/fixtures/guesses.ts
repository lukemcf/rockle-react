import { Guess } from '../../machines/game-machine'

const skip: Guess = {
  title: undefined,
  artist: undefined,
  titleCorrect: false,
  artistCorrect: false
}

const titleCorrect: Guess = {
  title: 'Correct title',
  artist: 'Incorrect artist',
  titleCorrect: true,
  artistCorrect: false
}

const artistCorrect: Guess = {
  title: 'Incorrect title',
  artist: 'Correct artist',
  titleCorrect: false,
  artistCorrect: true
}

const incorrect: Guess = {
  title: 'Incorrect title',
  artist: 'Incorrect artist',
  titleCorrect: false,
  artistCorrect: true
}

const correct: Guess = {
  title: 'Correct title',
  artist: 'Correct artist',
  titleCorrect: true,
  artistCorrect: true
}

export { skip, titleCorrect, artistCorrect, incorrect, correct }
