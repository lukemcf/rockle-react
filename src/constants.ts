import { DateTime } from 'luxon'

export const getStartDate = () => DateTime.local(2022, 4, 17)
export const snippetLengths = [5, 10, 20, 35, 60]
export const playTimeoutSeconds = 10
