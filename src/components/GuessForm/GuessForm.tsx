import React, { useState } from 'react'
import styled from 'styled-components'
import {
  EuiButton,
  EuiComboBox,
  EuiComboBoxOptionOption,
  EuiFlexGroup,
  EuiFlexItem,
  EuiSpacer,
} from '@elastic/eui'
import { songOptions } from './GuessForm.utils'
import { snippetLengths } from '../../constants'

interface Props {
  className?: string
  onSubmit: (title?: string, artist?: string) => void
  numGuesses: number
}

const GuessFormWrapper = styled.form`
  && {
    .euiComboBox__input > input {
      font-size: 16px;
    }
  }
`

const GuessForm: React.FunctionComponent<Props> = ({
  className,
  numGuesses,
  onSubmit,
}) => {
  const [selectedOptions, setSelectedOptions] = useState<
    EuiComboBoxOptionOption[]
  >([])

  const currentSnippetLength = snippetLengths[numGuesses] || 0
  const nextSnippetLength = snippetLengths[numGuesses + 1]
  const snippetLengthIncrease =
    nextSnippetLength && nextSnippetLength - currentSnippetLength

  const handleSubmit = (e: React.SyntheticEvent) => {
    const selectedOption = selectedOptions[0]
    if (selectedOption?.value) {
      const [artist, title] = selectedOption?.value as string[]

      onSubmit(artist, title)
      setSelectedOptions([])
    }

    e.preventDefault()
  }

  const skip = () => {
    onSubmit(undefined, undefined)
    setSelectedOptions([])
  }

  return (
    <GuessFormWrapper className={className} onSubmit={handleSubmit}>
      <EuiFlexGroup direction="column" gutterSize="xs">
        <EuiFlexItem>
          <EuiComboBox
            fullWidth
            placeholder="Select song"
            singleSelection={{ asPlainText: true }}
            selectedOptions={selectedOptions}
            options={songOptions}
            onChange={setSelectedOptions}
            aria-label="Song"
          />
        </EuiFlexItem>
        <EuiSpacer size="s" />
        <EuiFlexGroup justifyContent="center" responsive={false}>
          <EuiFlexItem grow={false}>
            <EuiButton size="s" color="warning" onClick={skip} data-testid="skip-btn">
              Skip
              {snippetLengthIncrease ? ` (+${snippetLengthIncrease} secs)` : ''}
            </EuiButton>
          </EuiFlexItem>
          <EuiFlexItem grow={false}>
            <EuiButton type="submit" size="s" disabled={selectedOptions.length === 0}>
              Guess
            </EuiButton>
          </EuiFlexItem>
        </EuiFlexGroup>
      </EuiFlexGroup>
    </GuessFormWrapper>
  )
}

export default GuessForm
