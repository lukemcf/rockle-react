import songs, { Song }  from '../../data/songs'
import { EuiComboBoxOptionOption } from '@elastic/eui'

const buildOption = (song: Song): EuiComboBoxOptionOption => ({
  label: `${song.artist} - ${song.title}`,
  value: [song.artist, song.title]
})

const byArtistThenTitle = (song1: Song, song2: Song) => {
  return song1.artist.localeCompare(song2.artist) || song1.title.localeCompare(song2.title)
}

const sortedSongs = [...songs].sort(byArtistThenTitle)
const songOptions: EuiComboBoxOptionOption[] = sortedSongs.map(buildOption)

export { songOptions }
