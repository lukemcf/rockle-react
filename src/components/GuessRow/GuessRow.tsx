import React from 'react'
import { BlankColumn, GuessColumn, Row, SkipColumn } from './GuessRow.components'
interface Props {
  title?: string
  artist?: string
  titleCorrect: boolean
  artistCorrect: boolean
}

const GuessRow: React.FunctionComponent<Props> = ({
  title,
  artist,
  titleCorrect,
  artistCorrect,
}) => {
  return (
    <Row
      dataTestId='guess-row'
      leftColumn={(
        <>
          {artist && (
            <GuessColumn correct={artistCorrect} dataTestId="guessed-artist">
              {artist}
            </GuessColumn>
          )}
          {!artist && <SkipColumn dataTestId="skipped-artist" />}
        </>
      )}
      rightColumn={(
        <>
          {title && (
            <GuessColumn correct={titleCorrect} dataTestId="guessed-title">
              {title}
            </GuessColumn>
          )}
          {!title && <SkipColumn dataTestId="skipped-title" />}
        </>
      )}
    />
  )
}

const BlankRow: React.FunctionComponent<{}> = () => (
  <Row
    dataTestId='blank-row'
    leftColumn={<BlankColumn />}
    rightColumn={<BlankColumn />}
  />
)

export { BlankRow }
export default GuessRow
