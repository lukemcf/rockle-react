import React from 'react'
import styled from 'styled-components'
import { EuiFlexGroup, EuiFlexItem, EuiIcon, EuiPanel } from '@elastic/eui'

interface GuessIconProps {
  correct: boolean
}

const GuessIcon: React.FunctionComponent<GuessIconProps> = ({ correct }) => (
  <EuiIcon type={correct ? 'checkInCircleFilled' : 'crossInACircleFilled'} />
)

const Column = styled(EuiPanel)`
  && {
    min-height: 33px;
  }
`

interface GuessColumnProps {
  correct: boolean
  dataTestId?: string
  children: React.ReactNode
}

const GuessColumn: React.FunctionComponent<GuessColumnProps> = ({
  correct,
  dataTestId,
  children,
}) => (
  <Column
    hasBorder
    paddingSize="s"
    color={correct ? 'success' : 'danger'}
    data-testid={dataTestId}>
    <GuessIcon correct={correct} /> {children}
  </Column>
)

const SkipColumn: React.FunctionComponent<{ dataTestId?: string }> = ({
  dataTestId,
}) => (
  <Column
    hasBorder
    paddingSize="s"
    color={'warning'}
    data-testid={dataTestId}>
    <EuiIcon type="crossInACircleFilled" />
  </Column>
)

const BlankColumn: React.FunctionComponent<{}> = () => (
  <Column
    hasBorder
    paddingSize="s"
    color={'plain'}
    data-testid='blank-column' />
)

interface RowProps {
  dataTestId?: string,
  leftColumn: React.ReactNode,
  rightColumn: React.ReactNode,
}

const Row: React.FunctionComponent<RowProps> = ({ dataTestId, leftColumn, rightColumn }) => (
  <EuiFlexGroup wrap={false} responsive={false} gutterSize="s" data-testid={dataTestId}>
    <EuiFlexItem>
      {leftColumn}
    </EuiFlexItem>
    <EuiFlexItem>
      {rightColumn}
    </EuiFlexItem>
  </EuiFlexGroup>
)

export { BlankColumn, GuessColumn, SkipColumn, Row }
