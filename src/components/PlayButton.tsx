import React from 'react'
import { PlayerContext } from './Player'
import { EuiButton } from '@elastic/eui'

interface Props {
  className?: string
  children: React.ReactNode
}

const PlayButton: React.FunctionComponent<Props> = ({
  className,
  children,
}) => {
  return (
    <PlayerContext.Consumer>
      {({ loading, player, playing, playSnippet }) => (
        <EuiButton
          className={className}
          isDisabled={playing}
          isLoading={!player || loading}
          onClick={playSnippet}
          iconType="play"
          data-testid="play-button">
          {children}
        </EuiButton>
      )}
    </PlayerContext.Consumer>
  )
}

export default PlayButton
