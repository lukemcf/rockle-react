import React from 'react'
import styled from 'styled-components'

interface Props {
  visible: boolean
}

const PlayerTargetWrapper = styled.div`
  position: ${(props: Props) => (props.visible ? 'unset' : 'absolute')};
  top: ${(props: Props) => (props.visible ? 'unset' : '-1000px')};
  margin-top: 20px;
`

const playerTargetId = 'player-target'
const PlayerTarget: React.FunctionComponent<Props> = ({ visible }) => {
  return (
    <PlayerTargetWrapper visible={visible} data-testid="player-target">
      <div id={playerTargetId} />
    </PlayerTargetWrapper>
  )
}

export { playerTargetId }

export default PlayerTarget
