import { Song } from '../../data/songs'
declare global {
  interface Window {
    onYouTubeIframeAPIReady: Function
  }
}

export interface Props {
  snippetLength: number,
  gameFinished: boolean,
  children: React.ReactNode
  song: Song,
}

export interface Context {
  loading: boolean
  player: YT.Player | undefined
  play: () => Promise<void>
  playing: boolean
  playSnippet: () => Promise<void>
}

