import { createContext } from 'react'
import { Context } from './Player.types'

const PlayerContext = createContext<Context>({
  player: undefined,
  loading: false,
  playing: false,
  play: () => Promise.reject('player not loaded'),
  playSnippet: () => Promise.reject('player not loaded'),
})

export default PlayerContext
