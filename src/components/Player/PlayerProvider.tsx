import React, { useCallback, useEffect, useState } from 'react'
import PlayerContext from './PlayerContext'
import loadScript from '../../utils/load-script'
import { playerTargetId } from './PlayerTarget'
import { Props } from './Player.types'
import { playTimeoutSeconds } from '../../constants'

const useAsyncError = () => {
  const [_, setError] = useState()
  return useCallback(
    (e) => {
      setError(() => {
        throw e
      })
    },
    [setError],
  )
}

const PlayerProvider: React.FunctionComponent<Props> = ({
  gameFinished,
  song,
  snippetLength,
  children,
}) => {
  const [player, setPlayer] = useState<YT.Player | undefined>()
  const [loading, setLoading] = useState<boolean>(false)
  const [state, setState] = useState<YT.PlayerState | undefined>()
  const throwError = useAsyncError()

  const playing = typeof YT !== 'undefined' && state === YT.PlayerState.PLAYING

  const initializePlayer = () => {
    new YT.Player(playerTargetId, {
      height: '390',
      width: '100%',
      videoId: song.videoId,
      playerVars: {
        playsinline: 1,
        start: song.start,
      },
      events: {
        onReady: (event) => {
          setPlayer(event.target)
        },
        onStateChange: (event) => {
          setState(event.target.getPlayerState())
        },
      },
    })
  }

  const play = useCallback(
    () =>
      new Promise<void>((resolve) => {
        if (typeof YT === 'undefined') return
        if (player?.getPlayerState() === YT.PlayerState.PLAYING) return

        setLoading(true)
        player?.playVideo()

        let timeoutId: NodeJS.Timer
        let intervalId: NodeJS.Timer

        timeoutId = setTimeout(() => {
          clearTimeout(timeoutId)
          clearTimeout(intervalId)
          throwError(new Error('play failed'))
        }, playTimeoutSeconds * 1000)

        intervalId = setInterval(() => {
          console.log('waiting for player to start...')
          if (
            player?.getPlayerState() ===
            (YT.PlayerState.PLAYING as YT.PlayerState)
          ) {
            console.log('started')
            console.log('seeking to:', song.start)
            player?.seekTo(song.start || 0, true)
            setLoading(false)
            resolve()
            clearTimeout(timeoutId)
            clearInterval(intervalId)
          }
        }, 100)
      }),
    [player, throwError, song.start],
  )

  const playSnippet = async () => {
    console.log('playSnippet', player)
    if (!player) return

    await play()
    console.log('started playing', player)

    setTimeout(() => player?.stopVideo(), snippetLength * 1000)
  }

  useEffect(() => {
    loadScript('https://www.youtube.com/iframe_api')
    window.onYouTubeIframeAPIReady = () => initializePlayer()
  })

  useEffect(() => {
    if (gameFinished) play()
  }, [gameFinished, play])

  return (
    <PlayerContext.Provider
      value={{
        loading,
        play,
        player,
        playing,
        playSnippet,
      }}>
      {children}
    </PlayerContext.Provider>
  )
}

export default PlayerProvider
