import React from 'react'
import styled from 'styled-components'
import { EuiTitle } from '@elastic/eui'
import { ReactComponent as Logo } from '../../images/logo.svg'

interface Props {
  className?: string
  gameNumber: number
}

const StyledLogo = styled(Logo)`
  margin: -15px auto;
  width: 200px;
  fill: azure;
`

const StyledH2 = styled.h2`
  text-align: center;
  font-size: 16px;
  && {
    margin-top: 0;
  }
`

const Title: React.FunctionComponent<Props> = ({ gameNumber }) => (
  <>
    <EuiTitle data-testid="app-title">
      <StyledLogo />
    </EuiTitle>
    <EuiTitle data-testid="game-number">
      <StyledH2>#{gameNumber}</StyledH2>
    </EuiTitle>
  </>
)

export default Title
