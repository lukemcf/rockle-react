import React from 'react'
import { Guess } from '../../machines/game-machine'
import { getShareData } from './Sharing.utils'
import canShare from '../../utils/can-share'
import share from '../../utils/share'

interface Props {
  gameNumber: number
  guesses: Guess[]
  children: (canShare: boolean, share: () => Promise<void>) => React.ReactNode
}

const Sharing: React.FunctionComponent<Props> = ({ gameNumber, guesses, children }) => {
  const shareData = getShareData(gameNumber, guesses)

  return <>{children(canShare(shareData), () => share(shareData))}</>
}

export default Sharing
