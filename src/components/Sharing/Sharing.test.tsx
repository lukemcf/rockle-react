import Sharing from './Sharing'
import { fireEvent, render, screen } from '@testing-library/react'
import { Guess } from '../../machines/game-machine'
import * as canShareUtil from '../../utils/can-share'
import * as shareUtil from '../../utils/share'
import * as sharingUtils from './Sharing.utils'

const renderWithTestChildFn = (guesses: Guess[]) =>
  render(
    <Sharing gameNumber={123} guesses={guesses}>
      {(canShare, share) => (
        <div data-testid="children">
          {`canShare equals ${canShare}`}
          <button onClick={share}>Share</button>
        </div>
      )}
    </Sharing>,
  )

describe('Sharing', () => {
  const guesses: Guess[] = []

  describe('share api not supported', () => {
    beforeEach(() => {
      Object.defineProperty(navigator, 'canShare', { value: undefined })
      Object.defineProperty(navigator, 'share', { value: undefined })
    })

    it('renders child function with canShare equalling false', () => {
      renderWithTestChildFn(guesses)

      expect(screen.getByTestId('children')).toHaveTextContent(
        'canShare equals false',
      )
    })
  })

  describe('share api supported', () => {
    let canShareMock: jest.SpyInstance<boolean, [data?: ShareData | undefined]>
    let shareMock: jest.SpyInstance<
      Promise<void>,
      [data?: ShareData | undefined]
    >
    const testShareData = {
      text: 'foo bar baz',
      title: 'Rockle',
      url: 'https://www.example.com',
    }

    beforeEach(() => {
      canShareMock = jest.spyOn(canShareUtil, 'default')
      shareMock = jest.spyOn(shareUtil, 'default')
      jest.spyOn(sharingUtils, 'getShareData').mockReturnValue(testShareData)
    })

    describe('navigator.canShare returns false', () => {
      beforeEach(() => {
        canShareMock.mockReturnValue(false)
      })

      it('renders child function with canShare equalling false', () => {
        renderWithTestChildFn(guesses)

        expect(screen.getByTestId('children')).toHaveTextContent(
          'canShare equals false',
        )
      })
    })

    describe('navigator.canShare returns true', () => {
      beforeEach(() => {
        canShareMock.mockReturnValue(true)
      })

      it('renders child function with canShare equalling true', () => {
        renderWithTestChildFn(guesses)

        expect(screen.getByTestId('children')).toHaveTextContent(
          'canShare equals true',
        )
      })

      it('can share', () => {
        renderWithTestChildFn(guesses)

        fireEvent.click(screen.getByText('Share'))
        expect(shareMock).toHaveBeenCalledWith(testShareData)
      })
    })
  })
})
