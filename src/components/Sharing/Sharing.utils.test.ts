import { getShareData } from './Sharing.utils'
import { Guess } from '../../machines/game-machine'
import * as getCurrentUrl from '../../utils/get-current-url'

describe('getShareData', () => {
  let shareData: ShareData
  let guesses: Guess[] = []
  const gameNumber = 1234

  beforeEach(() => {
    jest.spyOn(getCurrentUrl, 'default').mockReturnValue('https://rockle.example.com')
    shareData = getShareData(gameNumber, guesses)
  })

  it('returns correct url & title', () => {
    expect(shareData.url).toEqual('https://rockle.example.com')
    expect(shareData.title).toEqual('Rockle')
  })

  describe('.text', () => {
    guesses = [{
      title: 'foo', artist: 'bar', titleCorrect: false, artistCorrect: false
    }, {
      title: 'foo', artist: 'bar', titleCorrect: false, artistCorrect: true
    }, {
      title: undefined, artist: undefined, titleCorrect: false, artistCorrect: false
    }, {
      title: 'foo', artist: 'bar', titleCorrect: true, artistCorrect: true
    }]

    it('describes winning game', () => {
      expect(shareData.text).toEqual(
        '🎸 Rockle #1234\n\n🟥🟥\n🟩🟥\n🟨🟨\n🟩🟩\n⬛️⬛️\n'
      )
    })
  })
})
