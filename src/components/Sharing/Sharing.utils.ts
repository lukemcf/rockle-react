import { Guess } from '../../machines/game-machine'
import getCurrentUrl from '../../utils/get-current-url'
import { snippetLengths } from '../../constants'

const Symbols = {
  EMPTY: '⬛️',
  CORRECT: '🟩',
  PARTIALLY_CORRECT: '🟨',
  INCORRECT: '🟥'
}

const getTitle = (): string => 'Rockle'
const getSymbols = (guess: Guess): string => {
  const { title, artist, titleCorrect, artistCorrect } = guess

  if (!title && !artist) return `${Symbols.PARTIALLY_CORRECT}${Symbols.PARTIALLY_CORRECT}`
  if (titleCorrect && artistCorrect) return `${Symbols.CORRECT}${Symbols.CORRECT}`
  if (!titleCorrect && !artistCorrect) return `${Symbols.INCORRECT}${Symbols.INCORRECT}`
  if (titleCorrect && !artistCorrect) return `${Symbols.INCORRECT}${Symbols.CORRECT}`
  if (!titleCorrect && artistCorrect) return `${Symbols.CORRECT}${Symbols.INCORRECT}`

  return `${Symbols.EMPTY}${Symbols.EMPTY}`
}

const getText = (gameNumber: number, guesses: Guess[]): string => {
  const totalNumGuesses = snippetLengths.length
  const joinedGuesses = guesses.map(getSymbols).join('\n')
  const remainingGuesses = new Array(totalNumGuesses - guesses.length).fill(`${Symbols.EMPTY}${Symbols.EMPTY}`)
  const joinedRemainingGuesses = remainingGuesses.join('\n')

  return `🎸 Rockle #${gameNumber}\n\n${joinedGuesses}\n${joinedRemainingGuesses}\n`
}

const getShareData = (gameNumber: number, guesses: Guess[]): ShareData => {
  return {
    url: getCurrentUrl(),
    title: getTitle(),
    text: getText(gameNumber, guesses)
  }
}

export { getShareData }
