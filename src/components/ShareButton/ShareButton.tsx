import React from 'react'
import { EuiButton } from '@elastic/eui'

interface Props {
  onClick: () => void
}

const ShareButton: React.FunctionComponent<Props> = ({ onClick }) => {
  return (
    <EuiButton iconType="share" data-testid="share-button" onClick={onClick}>
      Share
    </EuiButton>
  )
}

export default ShareButton
