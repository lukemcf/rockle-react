import React from 'react'
import { EuiCard, EuiIcon } from '@elastic/eui'
import styled from 'styled-components'

const ErrorMessageWrapper = styled(EuiCard)`
  max-width: 600px;
  margin: 0 auto;
`
interface Props {
  title?: string,
  description?: string
}

const ErrorMessage: React.FunctionComponent<Props> = ({ title, description }) => (
  <ErrorMessageWrapper
    title={title || "Oops, that didn't go to plan"}
    description={description || "Please reload the page and try again"}
    display="danger"
    icon={<EuiIcon size="xxl" type="cross" />}
    data-testid="error-message"
  />
)

export default ErrorMessage
