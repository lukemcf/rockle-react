import React from 'react'
import ErrorMessage from '../ErrorMessage'

interface Props {
  children: React.ReactNode
}

interface State {
  hasError: boolean
  title?: string
  description?: string
}

class ErrorBoundary extends React.Component {
  state: State

  constructor(props: Props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError(error: Error) {
    let title, description

    if (error.message === 'no song') {
      title = "Show's over!"
      description = "We don't have a song for today's date just yet. Check again soon!"
    }

    return { hasError: true, title, description }
  }

  render() {
    const { hasError, title, description } = this.state

    if (hasError) {
      return <ErrorMessage title={title} description={description}/>
    }

    return this.props.children
  }
}

export default ErrorBoundary
