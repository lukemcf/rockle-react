import React from 'react'
import { EuiButtonIcon, EuiFlexGroup, EuiFlexItem } from '@elastic/eui'
import Title from '../Title'
import { mergeQueryParams } from '../../utils/query-params'

interface Props {
  debug: boolean
  gameNumber: number
}

const Header: React.FunctionComponent<Props> = ({ debug, gameNumber }) => (
  <EuiFlexGroup justifyContent="spaceBetween" responsive={false}>
    {debug && (
      <EuiFlexItem grow={false}>
        <EuiButtonIcon
          iconType="arrowLeft"
          aria-label="Previous"
          href={mergeQueryParams({ gameNumber: String(gameNumber - 1) })}
        />
      </EuiFlexItem>
    )}
    <EuiFlexItem>
      <Title gameNumber={gameNumber} />
    </EuiFlexItem>
    {debug && (
      <EuiFlexItem grow={false}>
        <EuiButtonIcon
          iconType="arrowRight"
          aria-label="Previous"
          href={mergeQueryParams({ gameNumber: String(gameNumber + 1) })}
        />
      </EuiFlexItem>
    )}
  </EuiFlexGroup>
)

export default Header
