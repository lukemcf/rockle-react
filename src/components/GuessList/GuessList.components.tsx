import React from 'react'
import { BlankRow } from '../GuessRow'
import styled from 'styled-components'

const GuessListWrapper = styled.ul`
  margin-top: 20px;
`

const GuessLineItem = styled.li`
  &:not(:first-child) {
    margin-top: 10px;
  }
`

const BlankRows: React.FunctionComponent<{ count: number }> = ({ count }) => {
  const arr = new Array(count).fill(null)

  return (
    <>
      {arr.map((_, index) => (
        <GuessLineItem key={index}>
          <BlankRow />
        </GuessLineItem>
      ))}
    </>
  )
}

export { BlankRows, GuessListWrapper, GuessLineItem }
