import React from 'react'
import { Guess } from '../../machines/game-machine'
import GuessRow from '../GuessRow'
import { BlankRows, GuessListWrapper, GuessLineItem } from './GuessList.components'
import { snippetLengths } from '../../constants'

interface Props {
  guesses: Guess[]
}

const GuessList: React.FunctionComponent<Props> = ({ guesses }) => {
  const totalNumGuesses = snippetLengths.length

  return (
    <GuessListWrapper>
      {guesses.map((guess, index) => (
        <GuessLineItem key={index}>
          <GuessRow
            title={guess.title}
            artist={guess.artist}
            titleCorrect={guess.titleCorrect}
            artistCorrect={guess.artistCorrect}
          />
        </GuessLineItem>
      ))}
      <BlankRows count={totalNumGuesses - guesses.length} />
    </GuessListWrapper>
  )
}

export default GuessList
