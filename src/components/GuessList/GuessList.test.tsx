import GuessList from './GuessList'
import { render, screen, within } from '@testing-library/react'
import * as guessFixtures from '../../tests/fixtures/guesses'

describe('GuessList', () => {
  const guesses = [
    guessFixtures.skip,
    guessFixtures.artistCorrect,
    guessFixtures.correct,
  ]

  it('renders list of guess rows', () => {
    render(<GuessList guesses={guesses} />)

    const guessRows = screen.getAllByTestId('guess-row')
    expect(guessRows).toHaveLength(3)
    expect(within(guessRows[0]).getByTestId('skipped-title')).toBeInTheDocument()
    expect(within(guessRows[0]).getByTestId('skipped-artist')).toBeInTheDocument()
    expect(within(guessRows[1]).getByTestId('guessed-title')).toHaveClass('euiPanel--danger')
    expect(within(guessRows[1]).getByTestId('guessed-artist')).toHaveClass('euiPanel--success')
    expect(within(guessRows[2]).getByTestId('guessed-title')).toHaveClass('euiPanel--success')
    expect(within(guessRows[2]).getByTestId('guessed-artist')).toHaveClass('euiPanel--success')

    expect(screen.getAllByTestId('blank-row')).toHaveLength(2)
  })
})
