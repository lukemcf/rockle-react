import React from 'react'
import { EuiIcon, EuiCard } from '@elastic/eui'
import Sharing from '../Sharing'
import ShareButton from '../ShareButton'
import { Guess } from '../../machines/game-machine'

interface Props {
  correctSong: { title: string; artist: string } | undefined
  className?: string
  gameNumber: number
  guesses: Guess[]
  won: boolean
}

const FinishedPanel: React.FunctionComponent<Props> = ({
  className,
  correctSong,
  gameNumber,
  guesses,
  won,
}) => {
  return (
    <EuiCard
      className={className}
      icon={<EuiIcon size="xxl" type={won ? 'check' : 'cross'} />}
      title={won ? 'Rock on!' : 'Better luck next time'}
      description={`The answer was ${correctSong?.artist} - ${correctSong?.title}`}
      data-testid="finished-panel"
      footer={
        <Sharing gameNumber={gameNumber} guesses={guesses}>
          {(canShare, share) => (
            <>
              {canShare && <ShareButton onClick={share} />}
            </>
          )}
        </Sharing>
      }
    />
  )
}

export default FinishedPanel
