import React, { useEffect } from 'react'
import { PlayerProvider, PlayerTarget } from '../Player'
import { useMachine } from '@xstate/react'
import gameMachine from '../../machines/game-machine'
import getSong from '../../utils/get-song'
import getGameNumber from '../../utils/get-game-number'
import { EuiPage, EuiPageBody, EuiProvider } from '@elastic/eui'
import Header from '../Header'
import GuessList from '../GuessList'
import { PageContent, PlayButtonWrapper, GuessFormWrapper, FinishedPanelWrapper } from './App.components'
import ErrorBoundary from '../ErrorBoundary'

import '@elastic/eui/dist/eui_theme_dark.css'

interface Props {
  queryParams: { [key: string]: string }
}


const App: React.FunctionComponent<Props> = ({ queryParams }) => {
  const debug: boolean = queryParams.debug === 'true'
  const gameNumber = Number(queryParams.gameNumber) || getGameNumber()
  const correctSong = getSong(gameNumber)

  const [state, send] = useMachine<typeof gameMachine>(gameMachine)

  useEffect(() => {
    send({ type: 'INITIALIZE', song: correctSong })
  }, [send, correctSong])

  const won = state.value === 'won'
  const lost = state.value === 'lost'
  const gameFinished = won || lost

  const { guesses, snippetLength } = state.context

  return (
    <div className="App">
      <EuiProvider colorMode="dark">
        <PlayerProvider gameFinished={gameFinished} snippetLength={snippetLength} song={correctSong}>
          <EuiPage paddingSize="none">
            <EuiPageBody>
              <PageContent
                paddingSize="m"
                hasShadow={false}
                verticalPosition="center"
                horizontalPosition="center">
                <Header gameNumber={gameNumber} debug={debug}/>
                {!gameFinished && (
                  <PlayButtonWrapper>
                    Play ({snippetLength} seconds)
                  </PlayButtonWrapper>
                )}
                {!gameFinished && (
                  <GuessFormWrapper
                    numGuesses={guesses.length}
                    onSubmit={(artist, title) => {
                      send({ type: 'GUESS', song: { title, artist } })
                    }}
                  />
                )}
                {gameFinished && (
                  <FinishedPanelWrapper
                    correctSong={correctSong}
                    gameNumber={gameNumber}
                    guesses={guesses}
                    won={won}
                  />
                )}
                <GuessList guesses={guesses} />
                <PlayerTarget visible={debug || gameFinished} />
              </PageContent>
            </EuiPageBody>
          </EuiPage>
        </PlayerProvider>
      </EuiProvider>
    </div>
  )
}

const withErrorBoundary = <P, >(WrappedComponent: React.FunctionComponent<P>) => (props: P) => (
  <ErrorBoundary>
    <WrappedComponent {...props} />
  </ErrorBoundary>
)

export default withErrorBoundary(App)
