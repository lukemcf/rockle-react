import styled from 'styled-components'
import FinishedPanel from '../FinishedPanel'
import { EuiPageContent } from '@elastic/eui'
import PlayButton from '../PlayButton'
import GuessForm from '../GuessForm'

const PageContent = styled(EuiPageContent)`
  && {
    width: 100%;
    max-width: 600px;
  }
`

const PlayButtonWrapper = styled(PlayButton)`
  display: block;
  margin: 20px auto 5px;
`

const GuessFormWrapper = styled(GuessForm)`
  margin-top: 10px;
`

const FinishedPanelWrapper = styled(FinishedPanel)`
  margin-top: 20px;
`

export { PageContent, PlayButtonWrapper, GuessFormWrapper, FinishedPanelWrapper }
