import { fireEvent, screen } from '@testing-library/react'

const ENTER_KEY_OPTIONS =  {
  key: 'Enter',
  code: 'Enter',
  charCode: 13,
}

const submitGuess = (artist: string, title: string) => {
  const songCombo = screen.getByRole('combobox', {
    name: /song/i,
  })
  fireEvent.change(songCombo, {
    target: {
      value: [artist, title].filter(Boolean).join(' - ')
    },
  })
  fireEvent.keyDown(songCombo, ENTER_KEY_OPTIONS)
  fireEvent.click(screen.getByText('Guess'))
}

const skip = () => {
  fireEvent.click(screen.getByText(/Skip/))
}

export { submitGuess, skip }
