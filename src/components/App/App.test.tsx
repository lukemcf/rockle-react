import React from 'react'
import { render, screen, within } from '@testing-library/react'
import App from './App'
import * as constants from '../../constants'
import { DateTime, Settings } from 'luxon'
import { submitGuess, skip } from './test-helpers'

jest.mock('../../data/songs', () => {
  return {
    __esModule: true,
    default: [
      { title: 'Song 1', artist: 'Artist 1', videoId: 'videoid1' },
      { title: 'Song 2', artist: 'Artist 2', videoId: 'videoid2' },
      { title: 'Song 3', artist: 'Artist 2', videoId: 'videoid3' },
    ],
  }
})

describe('App', () => {
  beforeEach(() => {
    Settings.now = () => new Date(2022, 0, 2).getTime()
    jest
      .spyOn(constants, 'getStartDate')
      .mockReturnValue(DateTime.local(2022, 1, 1))
  })

  it('plays a winning game', () => {
    render(<App queryParams={{}} />)

    expect(screen.getByTestId('game-number')).toHaveTextContent('#2')

    expect(screen.getByTestId('play-button')).toBeDisabled()
    expect(screen.getByTestId('play-button')).toHaveTextContent(
      'Play (5 seconds)',
    )

    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+5 secs)')

    let guesses, lastGuess, guessedTitle, guessedArtist

    // partially correct guess:
    submitGuess('Artist 2', 'Song 3')

    guesses = screen.getAllByTestId('guess-row')
    expect(guesses).toHaveLength(1)

    lastGuess = guesses[0]

    guessedTitle = within(lastGuess).getByTestId('guessed-title')
    expect(guessedTitle).toHaveTextContent('Song 3')
    expect(guessedTitle).toHaveClass('euiPanel--danger')

    guessedArtist = within(lastGuess).getByTestId('guessed-artist')
    expect(guessedArtist).toHaveTextContent('Artist 2')
    expect(guessedArtist).toHaveClass('euiPanel--success')

    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+10 secs)')

    // skip:
    skip()

    guesses = screen.getAllByTestId('guess-row')
    expect(guesses).toHaveLength(2)

    lastGuess = guesses[1]

    guessedTitle = within(lastGuess).getByTestId('skipped-title')
    expect(guessedTitle).toHaveClass('euiPanel--warning')

    guessedArtist = within(lastGuess).getByTestId('skipped-artist')
    expect(guessedArtist).toHaveClass('euiPanel--warning')

    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+15 secs)')

    // correct guess:
    submitGuess('Artist 2', 'Song 2')

    guesses = screen.getAllByTestId('guess-row')
    expect(guesses).toHaveLength(3)

    lastGuess = guesses[2]

    guessedTitle = within(lastGuess).getByTestId('guessed-title')
    expect(guessedTitle).toHaveTextContent('Song 2')
    expect(guessedTitle).toHaveClass('euiPanel--success')

    guessedArtist = within(lastGuess).getByTestId('guessed-artist')
    expect(guessedArtist).toHaveTextContent('Artist 2')
    expect(guessedArtist).toHaveClass('euiPanel--success')

    expect(screen.getByTestId('finished-panel')).toHaveTextContent('Rock on!')
    expect(screen.getByTestId('finished-panel')).toHaveTextContent(
      'The answer was Artist 2 - Song 2',
    )

    expect(screen.getByTestId('player-target')).toHaveStyle("position: unset")
    expect(screen.getByTestId('player-target')).toHaveStyle("top: unset")
  })

  it('plays a losing game', () => {
    render(<App queryParams={{}} />)

    expect(screen.getByTestId('game-number')).toHaveTextContent('#2')

    expect(screen.getByTestId('play-button')).toBeDisabled()
    expect(screen.getByTestId('play-button')).toHaveTextContent(
      'Play (5 seconds)',
    )
    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+5 secs)')

    let guesses, lastGuess, guessedTitle, guessedArtist

    // incorrect guess:
    submitGuess('Artist 1', 'Song 1')

    guesses = screen.getAllByTestId('guess-row')
    expect(guesses).toHaveLength(1)

    lastGuess = guesses[0]

    guessedTitle = within(lastGuess).getByTestId('guessed-title')
    expect(guessedTitle).toHaveTextContent('Song 1')
    expect(guessedTitle).toHaveClass('euiPanel--danger')

    guessedArtist = within(lastGuess).getByTestId('guessed-artist')
    expect(guessedArtist).toHaveTextContent('Artist 1')
    expect(guessedArtist).toHaveClass('euiPanel--danger')

    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+10 secs)')

    // skip all remaining turns:
    skip()
    expect(screen.getAllByTestId('guess-row')).toHaveLength(2)
    expect(screen.getByText('Guess')).toBeInTheDocument()
    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+15 secs)')
    skip()
    expect(screen.getAllByTestId('guess-row')).toHaveLength(3)
    expect(screen.getByText('Guess')).toBeInTheDocument()
    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip (+25 secs)')
    skip()
    expect(screen.getAllByTestId('guess-row')).toHaveLength(4)
    expect(screen.getByText('Guess')).toBeInTheDocument()
    expect(screen.getByTestId('skip-btn')).toHaveTextContent('Skip')
    skip()
    expect(screen.getAllByTestId('guess-row')).toHaveLength(5)
    expect(screen.queryByText('Guess')).not.toBeInTheDocument()

    expect(screen.getByTestId('finished-panel')).toHaveTextContent(
      'Better luck next time',
    )
    expect(screen.getByTestId('finished-panel')).toHaveTextContent(
      'The answer was Artist 2 - Song 2',
    )

    expect(screen.getByTestId('player-target')).toHaveStyle("position: unset")
    expect(screen.getByTestId('player-target')).toHaveStyle("top: unset")
  })

  it('overrides game number when query param set', () => {
    render(<App queryParams={{ gameNumber: '3'}} />)
    expect(screen.getByTestId('game-number')).toHaveTextContent('#3')
  })

  it('shows error message if song doesnt exist', () => {
    const spy = jest.spyOn(console, 'error').mockImplementation(() => {}) // workaround for jest logging the caught error :-(
    render(<App queryParams={{ gameNumber: '4'}} />)
    expect(screen.getByTestId('error-message')).toHaveTextContent(
      "Show's over!We don't have a song for today's date just yet. Check again soon!"
    )
    spy.mockRestore()
  })

  it('shows prev/next buttons when in debug mode', () => {
    render(<App queryParams={{ debug: 'true' }} />)
  })
})
