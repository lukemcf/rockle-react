import GameMachine, { Guess }  from './game-machine'

export type { Guess }

export default GameMachine
