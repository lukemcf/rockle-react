import gameMachine from './game-machine'
import { interpret, InterpreterFrom } from 'xstate'
import { snippetLengths } from '../../constants'

const correctSong = { title: 'Erase/Replace', artist: 'Foo Fighters' }
const incorrectSong = { title: 'Incorrect title', artist: 'Incorrect artist' }
const incorrectTitle = { title: 'Incorrect title', artist: correctSong.artist }
const incorrectArtist = { title: correctSong.title, artist: 'Incorrect artist' }
const skip = { title: undefined, artist: undefined }

describe('renders learn react link', () => {
  let interpreter: InterpreterFrom<typeof gameMachine>

  beforeEach(() => {
    interpreter = interpret(gameMachine)
    interpreter.start()
  })

  it('plays a winning game', () => {
    expect(interpreter.state.value).toEqual('uninitialized')
    interpreter.send({ type: 'INITIALIZE', song: correctSong })
    expect(interpreter.state.value).toEqual('playing')
    expect(interpreter.state.context.snippetLength).toEqual(snippetLengths[0])

    interpreter.send({ type: 'GUESS', song: correctSong })
    expect(interpreter.state.context.guesses).toEqual([
      { ...correctSong, titleCorrect: true, artistCorrect: true }
    ])
    expect(interpreter.state.value).toEqual('won')
  })

  it('plays a losing game', () => {
    expect(interpreter.state.value).toEqual('uninitialized')
    interpreter.send({ type: 'INITIALIZE', song: correctSong })
    expect(interpreter.state.value).toEqual('playing')
    expect(interpreter.state.context.snippetLength).toEqual(snippetLengths[0])

    interpreter.send({ type: 'GUESS', song: incorrectArtist })
    expect(interpreter.state.context.guesses).toEqual([
      { ...incorrectArtist, artistCorrect: false, titleCorrect: true }
    ])
    expect(interpreter.state.value).toEqual('playing')
    expect(interpreter.state.context.snippetLength).toEqual(snippetLengths[1])

    interpreter.send({ type: 'GUESS', song: incorrectTitle })
    expect(interpreter.state.context.guesses).toEqual([
      { ...incorrectArtist, artistCorrect: false, titleCorrect: true },
      { ...incorrectTitle, artistCorrect: true, titleCorrect: false }
    ])
    expect(interpreter.state.value).toEqual('playing')
    expect(interpreter.state.context.snippetLength).toEqual(snippetLengths[2])

    interpreter.send({ type: 'GUESS', song: incorrectSong })
    expect(interpreter.state.context.guesses).toEqual([
      { ...incorrectArtist, artistCorrect: false, titleCorrect: true },
      { ...incorrectTitle, artistCorrect: true, titleCorrect: false },
      { ...incorrectSong, artistCorrect: false, titleCorrect: false }
    ])
    expect(interpreter.state.value).toEqual('playing')
    expect(interpreter.state.context.snippetLength).toEqual(snippetLengths[3])

    interpreter.send({ type: 'GUESS', song: skip })
    expect(interpreter.state.context.guesses).toEqual([
      { ...incorrectArtist, artistCorrect: false, titleCorrect: true },
      { ...incorrectTitle, artistCorrect: true, titleCorrect: false },
      { ...incorrectSong, artistCorrect: false, titleCorrect: false },
      { ...skip, artistCorrect: false, titleCorrect: false }
    ])
    expect(interpreter.state.value).toEqual('playing')

    interpreter.send({ type: 'GUESS', song: skip })
    expect(interpreter.state.context.guesses).toEqual([
      { ...incorrectArtist, artistCorrect: false, titleCorrect: true },
      { ...incorrectTitle, artistCorrect: true, titleCorrect: false },
      { ...incorrectSong, artistCorrect: false, titleCorrect: false },
      { ...skip, artistCorrect: false, titleCorrect: false },
      { ...skip, artistCorrect: false, titleCorrect: false }
    ])
    expect(interpreter.state.value).toEqual('lost')
  })
})
