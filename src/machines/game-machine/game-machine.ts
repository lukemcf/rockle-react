import { assign, createMachine } from 'xstate'
import { snippetLengths } from '../../constants'

interface Song { title: string, artist: string }

export interface Guess {
  title: string | undefined
  artist: string | undefined
  titleCorrect: boolean,
  artistCorrect: boolean
}

interface GameContext {
  correctSong: Song | undefined,
  guesses: Guess[],
  snippetLength: number
}

type GameEvent =
  | { type: 'INITIALIZE', song: Song }
  | { type: 'GUESS', song: { title: string | undefined, artist: string | undefined } }

type GameState =
  | { value: 'uninitialized', context: GameContext }
  | { value: 'playing', context: GameContext }
  | { value: 'evaluating', context: GameContext }
  | { value: 'won', context: GameContext }
  | { value: 'lost', context: GameContext }

const maxAttempts = snippetLengths.length

const gameMachine =
createMachine<GameContext, GameEvent, GameState>({
  id: 'game-machine',
  initial: 'uninitialized',
  context: {
    correctSong: undefined,
    guesses: [],
    snippetLength: 0
  },
  states: {
    uninitialized: {
      on: {
        'INITIALIZE': {
          target: 'playing',
          actions: assign({
            correctSong: (_, event) => event.song,
            snippetLength: (context, event) => {
              return snippetLengths[0]
            }
          })
        }
      }
    },
    playing: {
      on: {
        'GUESS': {
          target: 'evaluating',
          actions: assign({
            guesses: (context, event) => {
              const song = event.song
              const correctSong = context.correctSong

              const guess = {
                ...event.song,
                titleCorrect: song.title === correctSong?.title,
                artistCorrect: song.artist === correctSong?.artist
              }

              return [...context.guesses, guess]
            },
            snippetLength: (context, event) => {
              const numGuesses = context.guesses.length

              return snippetLengths[numGuesses + 1]
            }
          })
        }
      }
    },
    evaluating: {
      always: [
        { target: 'won', cond: 'hasWon' },
        { target: 'lost', cond: 'maxAttempts' },
        { target: 'playing' }
      ]
    },
    won: {
      type: 'final',
    },
    lost: {
      type: 'final',
    },
  },
},
{
  guards: {
    hasWon: context => {
      const lastGuess = context.guesses[context.guesses.length - 1]
      const correctSong = context.correctSong

      return !!lastGuess && !!correctSong && correctSong?.title === lastGuess.title && correctSong?.artist === lastGuess.artist
    },
    maxAttempts: context => {
      return context.guesses.length === maxAttempts
    }
  }
})

export default gameMachine
